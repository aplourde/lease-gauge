//
// Created by Anthony Plourde on 2014-05-04.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "EditChoiceValueViewController.h"
#import "InfoViewController.h"


@implementation EditChoiceValueViewController {

}
@synthesize titleLabel;

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (IBAction)save:(id)sender {
    NSString *currentChoice = [self.choices objectAtIndex:(NSUInteger) self.choiceTableView.indexPathForSelectedRow.row];
    [self.navigationController popViewControllerAnimated:YES];
    [self.delegate performSelector:self.callback withObject:currentChoice];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.choices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChoiceCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChoiceCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        backgroundView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
        cell.backgroundColor = [UIColor clearColor];
        cell.backgroundView = backgroundView;
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:122.0/255.0 blue:1 alpha:0.6];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    cell.textLabel.text = [self.choices objectAtIndex:(NSUInteger) indexPath.row];
    return cell;
}

@end