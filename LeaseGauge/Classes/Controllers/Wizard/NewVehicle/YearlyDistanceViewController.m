//
//  YearlyDistanceViewController.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "YearlyDistanceViewController.h"
#import "WizardController.h"
#import "Vehicle.h"
#import "NSString+IsNumeric.h"

@interface YearlyDistanceViewController ()

@end

@implementation YearlyDistanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    kmButton.selected = YES;
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)yearlyDistanceChanged:(id)sender {
    [self setYearlyDistance];
}

- (void)setYearlyDistance {
    Vehicle *vehicle = controller.buildingObject;
    if (distanceTextField.text.length > 0 && [distanceTextField.text isNumeric]) {
        vehicle.allowedDistancePerYear = distanceTextField.text.integerValue;
    } else {
        vehicle.allowedDistancePerYear = 25000;
    }
}

- (IBAction)next:(id)sender {
    [self setYearlyDistance];
    [controller goForward];
}

- (IBAction)back:(id)sender {
    [controller goBack];
}

- (IBAction)miles:(id)sender {
    Vehicle *vehicle = controller.buildingObject;
    vehicle.distanceUnit = MILES;
    [milesButton setSelected:YES];
    [kmButton setSelected:NO];
}

- (IBAction)km:(id)sender {
    Vehicle *vehicle = controller.buildingObject;
    vehicle.distanceUnit = KM;
    [kmButton setSelected:YES];
    [milesButton setSelected:NO];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


@end
