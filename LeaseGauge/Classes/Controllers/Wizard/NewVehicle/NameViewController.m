//
//  NameViewController.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "NameViewController.h"
#import "Vehicle.h"
#import "WizardController.h"
#import "VehicleList.h"

@interface NameViewController ()

@end

@implementation NameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nameChanged:(id)sender {
    [self setName];
}

- (void)setName {
    Vehicle *vehicle = controller.buildingObject;
    if ([nameTextField.text length] > 0) {
        vehicle.name = nameTextField.text;
    } else {
        vehicle.name = [NSString stringWithFormat:@"Vehicle #%i", [[VehicleList sharedInstance] count] + 1];
    }
}

- (IBAction)next:(id)sender {
    [self setName];
    [controller goForward];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)back:(id)sender {
    [controller goBack];
}

@end
