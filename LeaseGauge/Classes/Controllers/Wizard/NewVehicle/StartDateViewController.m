//
//  StartDateViewController.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "StartDateViewController.h"
#import "WizardController.h"
#import "Vehicle.h"

@interface StartDateViewController ()

@end

@implementation StartDateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    startDatePicker.maximumDate = [NSDate date];
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startDateChanged:(id)sender {
    [self setStartDate];
}

- (void)setStartDate {
    Vehicle *vehicle = controller.buildingObject;
    vehicle.leaseStartDate = startDatePicker.date;
}

- (IBAction)done:(id)sender {
    [self setStartDate];
    [controller goForward];
}

- (IBAction)back:(id)sender {
    [controller goBack];
}

@end
