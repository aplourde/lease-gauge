//
//  WelcomeView.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardController.h"

@protocol WelcomeViewControllerDelegate
- (void)welcomeDone;
@end

@interface WelcomeViewController : UIViewController <WizardDelegate> {
    IBOutlet UILabel *titleLabel;
    IBOutlet UIButton *addButton;
}

@property(nonatomic, strong) id <WelcomeViewControllerDelegate> delegate;

- (IBAction) add:(id)sender;
@end
