//
//  DashboardViewController.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WelcomeViewController.h"

@class PieChart;

@interface DashboardViewController : UIViewController <WelcomeViewControllerDelegate, UIViewControllerTransitioningDelegate> {

    IBOutlet UILabel *vehicleNameLabel;

    IBOutlet UIScrollView *scrollView;

    IBOutlet UILabel *distanceTitleLabel;
    IBOutlet UIView *distanceSectionView;
    IBOutlet UIButton *addFirstJournalEntryButton;
    IBOutlet UILabel *lastReadLabel;
    IBOutlet UILabel *currentDistanceLabel;
    IBOutlet UILabel *distanceLeftLabel;
    IBOutlet UILabel *distancePercentageLabel;
    IBOutlet UILabel *statusLabel;
    IBOutlet PieChart *distancePieChart;

    IBOutlet UILabel *timeTitleLabel;
    IBOutlet UIView *timeSectionView;
    IBOutlet UILabel *todayTitleLabel;
    IBOutlet UILabel *todayDateLabel;
    IBOutlet UILabel *timeLeftLabel;
    IBOutlet UILabel *timePercentageLabel;
    IBOutlet PieChart *timePieChart;

    IBOutlet UILabel *normalUsageTitleLabel;
    IBOutlet UIView *normalUsageSectionView;
    IBOutlet UILabel *normalUsageIntroLabel;
    IBOutlet UILabel *averageLabel1;
    IBOutlet UILabel *averageLabel2;
    IBOutlet UILabel *averageLabel3;
    IBOutlet UILabel *averageLabel4;
    IBOutlet UILabel *averageLabel5;
    IBOutlet UILabel *averageLabel6;
    IBOutlet UILabel *averageUnitLabel;

}

- (IBAction)showEditView:(id)sender;

- (IBAction)showVehicleListView:(id)sender;
@end
