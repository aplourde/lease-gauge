//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "VehicleListViewController.h"
#import "VehicleList.h"
#import "Vehicle.h"
#import "NewVehicleWizard.h"
#import "DashboardViewController.h"


@implementation VehicleListViewController {
    NewVehicleWizard *wizard;
}

- (void)viewDidLoad {
}

- (void)viewWillAppear:(BOOL)animated {
    [vehicleTableView reloadData];
    [self selectCurrentVehicle];
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[VehicleList sharedInstance] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Vehicle *vehicle = [[VehicleList sharedInstance] vehicleAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VehicleCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VehicleCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:30];
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 66)];
        backgroundView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
        cell.backgroundColor = [UIColor clearColor];
        cell.backgroundView = backgroundView;
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 66)];
        selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:122.0/255.0 blue:1 alpha:0.6];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    cell.textLabel.text = [vehicle name];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    Vehicle *vehicle = [[VehicleList sharedInstance] vehicleAtIndex:indexPath.row];
    [[VehicleList sharedInstance] setCurrentVehicle:vehicle];
    [self done:nil];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[VehicleList sharedInstance] removeVehicleAtIndex:indexPath.row];

        [vehicleTableView beginUpdates];
        [vehicleTableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        [vehicleTableView endUpdates];
        [self selectCurrentVehicle];
    }
}

- (IBAction)add:(id)sender {
    Vehicle *vehicle = [[Vehicle alloc] init];
    wizard = [[NewVehicleWizard alloc] init];
    wizard.buildingObject = vehicle;
    wizard.wizardDelegate = self;
    [self presentViewController:wizard animated:YES completion:NULL];
}

- (IBAction)done:(id)sender {

    DashboardViewController *dashboardViewController = [[DashboardViewController alloc] initWithNibName:@"DashboardView" bundle:nil];
    [self.navigationController pushViewController:dashboardViewController animated:YES];
}

- (void)wizardDone:(Vehicle *)builtObject {
    [self dismissViewControllerAnimated:YES completion:NULL];
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:[self tableView:vehicleTableView numberOfRowsInSection:0] - 1 inSection:0];
    [vehicleTableView reloadData];
    [vehicleTableView selectRowAtIndexPath:newIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
}


- (void)wizardCanceled {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)selectCurrentVehicle {
    [vehicleTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[[VehicleList sharedInstance] currentVehicleIndex] inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

@end