//
// Created by Anthony Plourde on 2014-05-11.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (IsNumeric)

- (BOOL)isNumeric;
@end