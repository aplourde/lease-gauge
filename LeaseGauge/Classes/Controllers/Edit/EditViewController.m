//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "EditViewController.h"
#import "JournalViewController.h"
#import "InfoViewController.h"
#import "VehicleList.h"
#import "Vehicle.h"


@implementation EditViewController {

    JournalViewController *journalViewController;
    InfoViewController *infoViewController;
    UIViewController *currentViewController;
}

- (void)viewDidLoad {

    [self showJournalView];
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {

    vehicleName.text = [[[VehicleList sharedInstance] currentVehicle] name];
}

- (IBAction)showJournal:(id)sender {

    [self showJournalView];
}

- (IBAction)showInfo:(id)sender {

    [self showInfoView];
}

- (IBAction)close:(id)sender {

    [self showJournalView];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)showJournalView {

    if (!journalViewController) {
        journalViewController = [[JournalViewController alloc] initWithNibName:@"JournalView" bundle:nil];
    }
    [self showViewController:journalViewController insteadOf:infoViewController];
}

- (void)showInfoView {

    if (!infoViewController) {
        infoViewController = [[InfoViewController alloc] initWithNibName:@"InfoView" bundle:nil];
    }
    [self showViewController:infoViewController insteadOf:journalViewController];
}

- (void)showViewController:(UIViewController *)viewController insteadOf:(UIViewController *)otherViewController {

    if (currentViewController == viewController) {
        return;
    }
    [otherViewController.view removeFromSuperview];
    [otherViewController removeFromParentViewController];
    [contentView addSubview:viewController.view];
    [self addChildViewController:viewController];
    viewController.view.frame = contentView.bounds;
    currentViewController = viewController;
}



@end