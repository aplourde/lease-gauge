//
//  WizardController.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-30.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WizardDelegate

- (void)wizardDone:(id)builtObject;

- (void)wizardCanceled;
@end

@interface WizardController : UINavigationController {
    NSMutableArray *steps;
    id buildingObject;
    NSUInteger currentStepIndex;

    id <WizardDelegate> wizardDelegate;
}

@property(nonatomic, strong) id buildingObject;

@property(nonatomic, retain) id <WizardDelegate> wizardDelegate;

- (void)goForward;

- (void)goBack;

- (void)cancel;

- (void)done;
@end
