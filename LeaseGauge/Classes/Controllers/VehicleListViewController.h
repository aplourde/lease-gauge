//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WizardController.h"


@interface VehicleListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, WizardDelegate> {
    IBOutlet UITableView *vehicleTableView;
}
- (IBAction)add:(id)sender;
- (IBAction)done:(id)sender;
@end