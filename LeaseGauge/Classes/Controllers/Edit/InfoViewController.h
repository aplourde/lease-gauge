//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface InfoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *infoTableView;
}
@end