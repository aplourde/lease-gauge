//
//  LeaseTermViewController.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "DurationViewController.h"
#import "WizardController.h"
#import "Vehicle.h"
#import "NSString+IsNumeric.h"

@interface DurationViewController ()

@end

@implementation DurationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)next:(id)sender {
    [self setDuration];
    [controller goForward];
}

- (IBAction)back:(id)sender {
    [controller goBack];
}


- (IBAction)durationChanged:(id)sender {
    [self setDuration];
}

- (void)setDuration {
    Vehicle *vehicle = controller.buildingObject;
    if (durationTextField.text.length > 0 && [durationTextField.text isNumeric]) {
        vehicle.leaseDurationInMonths = durationTextField.text.integerValue;
    } else {
        vehicle.leaseDurationInMonths = 48;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
