//
// Created by Anthony Plourde on 2014-04-30.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JournalEntry;


@interface Journal : NSObject

- (void)addEntry:(JournalEntry *)entry;

- (id)initWithDictionaryArray:(NSArray*)dictArray;

- (JournalEntry *)latestEntry;

- (NSArray*)toDictionaryArray;

- (NSUInteger)entryCount;

- (JournalEntry *)entryAtIndex:(NSInteger)row;

- (void)removeEntryAtIndex:(NSInteger)index;
@end