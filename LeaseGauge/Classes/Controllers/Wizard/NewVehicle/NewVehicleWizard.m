//
//  NewVehicleWizard.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-30.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "NewVehicleWizard.h"
#import "Vehicle.h"
#import "NameViewController.h"
#import "YearlyDistanceViewController.h"
#import "DurationViewController.h"
#import "StartDateViewController.h"
#import "VehicleList.h"
#import "StartingDistanceViewController.h"


@implementation NewVehicleWizard

- (id)init {
    if (self) {
        steps = [[NSMutableArray alloc] init];
        NameViewController *nameViewController = [[NameViewController alloc] initWithNibName:@"NameView" andController:self];
        [steps addObject:nameViewController];

        self = [super initWithRootViewController:nameViewController];

        YearlyDistanceViewController *yearlyDistanceViewController = [[YearlyDistanceViewController alloc] initWithNibName:@"YearlyDistanceView" andController:self];
        [steps addObject:yearlyDistanceViewController];
        StartingDistanceViewController *startingDistanceViewController = [[StartingDistanceViewController alloc] initWithNibName:@"StartingDistanceView" andController:self];
        [steps addObject:startingDistanceViewController];
        DurationViewController *durationViewController = [[DurationViewController alloc] initWithNibName:@"DurationView" andController:self];
        [steps addObject:durationViewController];
        StartDateViewController *startDateViewController = [[StartDateViewController alloc] initWithNibName:@"StartDateView" andController:self];
        [steps addObject:startDateViewController];
    }
    return self;
}

- (void)done {

    Vehicle *vehicle = self.buildingObject;
    [[VehicleList sharedInstance] addVehicle:vehicle];

    [super done];
}


@end
