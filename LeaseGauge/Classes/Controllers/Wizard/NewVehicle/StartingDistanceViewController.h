//
//  YearlyDistanceViewController.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardStep.h"

@interface StartingDistanceViewController : WizardStep <UITextFieldDelegate> {
    IBOutlet UILabel *titleLabel;
    IBOutlet UITextField *distanceTextField;
    IBOutlet UIButton *nextButton;
}

- (IBAction)startingDistanceChanged:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)back:(id)sender;

@end
