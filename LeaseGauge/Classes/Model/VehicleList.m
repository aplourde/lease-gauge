//
//  VehicleList.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "VehicleList.h"
#import "Vehicle.h"

@implementation VehicleList {
    NSMutableArray *vehicles;
    NSString *currentVehicleId;
    Vehicle *currentVehicle;
}

- (id)init {
    self = [super init];
    if (self) {
        [self load];
        NSUbiquitousKeyValueStore *store = [NSUbiquitousKeyValueStore defaultStore];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(load)
                                                     name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                                   object:store];
        [store synchronize];
    }
    return self;
}

+ (VehicleList *)sharedInstance {
    static VehicleList *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (int)count {
    return [vehicles count];
}

- (NSArray *)all {
    return vehicles;
}

- (void)addVehicle:(Vehicle *)vehicle {

    vehicle.id = [self generateId];
    [vehicles addObject:vehicle];
    [self setCurrentVehicle:vehicle];
    [self save];
}

- (NSString *)generateId {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    NSString *uuidStr = (__bridge_transfer NSString *) CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    return uuidStr;
}

- (Vehicle *)currentVehicle {
    if (!currentVehicle) {
        if (currentVehicleId) {
            currentVehicle = [self vehicleWithId:currentVehicleId];
        }
    }
    return currentVehicle;
}

- (void)setCurrentVehicle:(Vehicle *)cv {
    currentVehicleId = cv.id;
    currentVehicle = cv;
}

- (Vehicle *)vehicleWithId:(NSString *)id {
    for (Vehicle *vehicle in vehicles) {
        if ([vehicle.id isEqualToString:id]) {
            return vehicle;
        }
    }
    return nil;
}

- (void)save {

    NSMutableArray *dictArray = [self toDictionaryArray];
    [[NSUbiquitousKeyValueStore defaultStore] setObject:dictArray forKey:@"vehicleList"];
    if (currentVehicleId) {
        [[NSUbiquitousKeyValueStore defaultStore] setObject:currentVehicleId forKey:@"currentVehicleId"];
    } else {
        [[NSUbiquitousKeyValueStore defaultStore] removeObjectForKey:@"currentVehicleId"];
    }
}

- (void)load {

    NSArray *listFromDefaults = [[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"vehicleList"];
    if (listFromDefaults) {
        vehicles = [[NSMutableArray alloc] initWithArray:[self fromDictionaryArray:listFromDefaults]];
        currentVehicleId = [[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"currentVehicleId"];
    } else {
        vehicles = [[NSMutableArray alloc] init];
    }
}

- (NSArray *)fromDictionaryArray:(NSArray *)dictArray {
    NSMutableArray *vehicleArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in dictArray) {
        Vehicle *vehicle = [[Vehicle alloc] initWithDictionary:dict];
        [vehicleArray addObject:vehicle];
    }
    return vehicleArray;
}

- (NSMutableArray *)toDictionaryArray {
    NSMutableArray *dictArray = [[NSMutableArray alloc] init];
    for (Vehicle *vehicle in vehicles) {
        NSDictionary *vehicleDict = [vehicle toDictionary];
        [dictArray addObject:vehicleDict];
    }
    return dictArray;
}

- (Vehicle *)vehicleAtIndex:(NSInteger)index {
    return [vehicles objectAtIndex:(NSUInteger) index];
}

- (NSInteger)currentVehicleIndex {
    return [vehicles indexOfObject:[self currentVehicle]];
}

- (void)removeVehicleAtIndex:(NSInteger)index {
    NSInteger selectedIndex = [self currentVehicleIndex];
    [vehicles removeObjectAtIndex:(NSUInteger) index];

    if (selectedIndex == index) {
        NSInteger newSelectedIndex = index;
        if (index >= vehicles.count) {
            newSelectedIndex--;
        }
        if (newSelectedIndex >= 0) {
            [self setCurrentVehicle:[vehicles objectAtIndex:(NSUInteger) newSelectedIndex]];
        } else {
            [self setCurrentVehicle:nil];
        }
    }
    [self save];
}
@end
