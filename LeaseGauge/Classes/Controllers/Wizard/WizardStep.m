//
//  WizardStep.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-30.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "WizardStep.h"
#import "WizardController.h"

@interface WizardStep ()

@end

@implementation WizardStep

- (id)initWithNibName:(NSString *)nibNameOrNil andController:(WizardController *)wizardController
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        controller = wizardController;
    }
    return self;
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-dark-leather"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
