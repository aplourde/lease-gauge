//
//  WizardStep.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-30.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WizardController;

@interface WizardStep : UIViewController {
    WizardController *controller;
}
- (id)initWithNibName:(NSString *)nibNameOrNil andController:(WizardController *)wizardController;
@end
