//
// Created by Anthony Plourde on 2014-05-04.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@class InfoViewController;


@interface EditDateValueViewController : UIViewController {

    IBOutlet UIButton *saveButton;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIDatePicker *datePicker;
}

@property(nonatomic, strong) UIDatePicker *datePicker;

@property(nonatomic, strong) id delegate;

@property(nonatomic) SEL callback;

@property(nonatomic, strong) UILabel *titleLabel;

- (IBAction)save:(id)sender;

@end