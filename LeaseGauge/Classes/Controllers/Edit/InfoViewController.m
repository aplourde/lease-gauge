//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "InfoViewController.h"
#import "Vehicle.h"
#import "VehicleList.h"
#import "EditTextValueViewController.h"
#import "EditDateValueViewController.h"
#import "EditChoiceValueViewController.h"


@implementation InfoViewController {

    NSArray *sectionsAndRows;
    EditTextValueViewController *editTextValueViewController;
    EditDateValueViewController *editDateValueViewController;
    EditChoiceValueViewController *editChoiceValueViewController;
}

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-dark-leather"]];
    infoTableView.scrollEnabled = YES;
    infoTableView.contentInset = UIEdgeInsetsMake(-44, 0, 0, 0);
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [self refreshData];
}

- (void)refreshData {
    Vehicle *vehicle = [[VehicleList sharedInstance] currentVehicle];
    sectionsAndRows = [NSArray arrayWithObjects:
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"", @"headerName",
                    [NSArray array], @"rows",
                    nil],
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"Vehicle Name", @"headerName",
                    [NSArray arrayWithObjects:
                            [NSDictionary dictionaryWithObjectsAndKeys:vehicle.name, @"value", @"name", @"field", nil],
                            nil], @"rows",
                    nil],
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"Yearly Distance", @"headerName",
                    [NSArray arrayWithObjects:
                            [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%i", vehicle.allowedDistancePerYear], @"value", @"allowedDistancePerYear", @"field", nil],
                            nil], @"rows",
                    nil],
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"Lease Duration", @"headerName",
                    [NSArray arrayWithObjects:
                            [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%i", vehicle.leaseDurationInMonths], @"value", @"leaseDurationInMonths", @"field", nil],
                            nil], @"rows",
                    nil],
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"Lease Start Date", @"headerName",
                    [NSArray arrayWithObjects:
                            [NSDictionary dictionaryWithObjectsAndKeys:[self formatDate:vehicle.leaseStartDate], @"value", @"leaseStartDate", @"field", nil],
                            nil], @"rows",
                    nil],
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"Odometer at Start", @"headerName",
                    [NSArray arrayWithObjects:
                            [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%i", vehicle.startingDistance], @"value", @"startingDistance", @"field", nil],
                            nil], @"rows",
                    nil],
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"Distance Unit", @"headerName",
                    [NSArray arrayWithObjects:
                            [NSDictionary dictionaryWithObjectsAndKeys:vehicle.distanceUnit == KM ? @"KM" : @"Miles", @"value", @"distanceUnit", @"field", nil],
                            nil], @"rows",
                    nil],
            nil];
    [infoTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sectionsAndRows count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [(NSArray*)[[sectionsAndRows objectAtIndex:(NSUInteger) section] objectForKey:@"rows"] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[sectionsAndRows objectAtIndex:(NSUInteger) section] objectForKey:@"headerName"];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 44)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:18];
    headerLabel.frame = headerView.frame;
    headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    [headerView addSubview:headerLabel];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditInfoCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EditInfoCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        backgroundView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
        cell.backgroundColor = [UIColor clearColor];
        cell.backgroundView = backgroundView;
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:122.0/255.0 blue:1 alpha:0.6];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    NSDictionary *section = [sectionsAndRows objectAtIndex:(NSUInteger) indexPath.section];
    NSDictionary *row = [[section objectForKey:@"rows"] objectAtIndex:(NSUInteger) indexPath.row];
    cell.textLabel.text = [row objectForKey:@"value"];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary *section = [sectionsAndRows objectAtIndex:(NSUInteger) indexPath.section];
    NSDictionary *row = [[section objectForKey:@"rows"] objectAtIndex:(NSUInteger) indexPath.row];
    Vehicle *vehicle = [[VehicleList sharedInstance] currentVehicle];
    NSString *field = [row objectForKey:@"field"];
    NSString *title = [section objectForKey:@"headerName"];

    if ([field isEqualToString:@"name"]) {
        [self showTextEditViewFor:field withTitle:title value:vehicle.name andCallback:@selector(saveName:)];
    } else if ([field isEqualToString:@"leaseStartDate"]) {
        [self showDateEditViewFor:field withTitle:title value:vehicle.leaseStartDate andCallback:@selector(saveLeaseStartDate:)];
    } else if ([field isEqualToString:@"allowedDistancePerYear"]) {
        [self showTextEditViewFor:field withTitle:title value:[NSNumber numberWithInteger:vehicle.allowedDistancePerYear] andCallback:@selector(saveAllowedDistancePerYear:)];
    } else if ([field isEqualToString:@"distanceUnit"]) {
        [self showChoiceEditViewFor:field withTitle:title value:vehicle.distanceUnit andCallback:@selector(saveDistanceUnit:)];
    } else if ([field isEqualToString:@"leaseDurationInMonths"]) {
        [self showTextEditViewFor:@"name" withTitle:title value:[NSNumber numberWithInteger:vehicle.leaseDurationInMonths] andCallback:@selector(saveLeaseDurationInMonth:)];
    } else if ([field isEqualToString:@"startingDistance"]) {
        [self showTextEditViewFor:@"startingDistance" withTitle:title value:[NSNumber numberWithInteger:vehicle.startingDistance] andCallback:@selector(saveStartingDistance:)];
    }
}

- (void)showChoiceEditViewFor:(NSString *)field withTitle:(NSString*)title value:(NSInteger)value andCallback:(SEL)callback {
    if (!editChoiceValueViewController) {
        editChoiceValueViewController = [[EditChoiceValueViewController alloc] initWithNibName:@"EditChoiceValueView" bundle:nil];
        [editChoiceValueViewController loadView];
        editChoiceValueViewController.delegate = self;
    }
    if ([field isEqualToString:@"distanceUnit"]) {
        editChoiceValueViewController.choices = @[@"KM", @"Miles"];
    }
    editChoiceValueViewController.callback = callback;
    editChoiceValueViewController.titleLabel.text = title;
    [self.parentViewController.navigationController pushViewController:editChoiceValueViewController animated:YES];
    [editChoiceValueViewController.choiceTableView reloadData];
    [editChoiceValueViewController.choiceTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:value inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void)showDateEditViewFor:(NSString *)field withTitle:(NSString*)title value:(NSDate *)value andCallback:(SEL)callback {
    if (!editDateValueViewController) {
        editDateValueViewController = [[EditDateValueViewController alloc] initWithNibName:@"EditDateValueView" bundle:nil];
        [editDateValueViewController loadView];
        editDateValueViewController.delegate = self;
    }
    editDateValueViewController.datePicker.datePickerMode = UIDatePickerModeDate;
    editDateValueViewController.datePicker.date = value;
    editDateValueViewController.datePicker.maximumDate = [NSDate date];
    editDateValueViewController.callback = callback;
    editDateValueViewController.titleLabel.text = title;
    [self.parentViewController.navigationController pushViewController:editDateValueViewController animated:YES];
}

- (void)showTextEditViewFor:(NSString *)string withTitle:(NSString*)title value:(id)value andCallback:(SEL)callback {
    if (!editTextValueViewController) {
        editTextValueViewController = [[EditTextValueViewController alloc] initWithNibName:@"EditTextValueView" bundle:nil];
        [editTextValueViewController loadView];
        editTextValueViewController.delegate = self;
    }
    if ([value isKindOfClass:[NSNumber class]]) {
        editTextValueViewController.valueTextField.keyboardType = UIKeyboardTypeNumberPad;
    } else {
        editTextValueViewController.valueTextField.keyboardType = UIKeyboardTypeDefault;
    }
    editTextValueViewController.valueTextField.text = [NSString stringWithFormat:@"%@", value];
    editTextValueViewController.callback = callback;
    editTextValueViewController.titleLabel.text = title;
    [self.parentViewController.navigationController pushViewController:editTextValueViewController animated:YES];
}


- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    return [dateFormatter stringFromDate:date];
}

- (void)saveName:(NSString *)newName {
    VehicleList *vehicles = [VehicleList sharedInstance];
    Vehicle *vehicle = [vehicles currentVehicle];
    vehicle.name = newName;
    [self refreshData];
    [vehicles save];
}

- (void)saveStartingDistance:(NSString *)startingDistance {
    VehicleList *vehicles = [VehicleList sharedInstance];
    Vehicle *vehicle = [vehicles currentVehicle];
    vehicle.startingDistance = [startingDistance integerValue];
    [self refreshData];
    [vehicles save];
}

- (void)saveLeaseDurationInMonth:(NSString *)duration {
    VehicleList *vehicles = [VehicleList sharedInstance];
    Vehicle *vehicle = [vehicles currentVehicle];
    vehicle.leaseDurationInMonths = [duration integerValue];
    [self refreshData];
    [vehicles save];
}

- (void)saveDistanceUnit:(NSString *)distanceUnit {
    VehicleList *vehicles = [VehicleList sharedInstance];
    Vehicle *vehicle = [vehicles currentVehicle];
    vehicle.distanceUnit = [distanceUnit isEqualToString:@"KM"] ? KM : MILES;
    [self refreshData];
    [vehicles save];
}

- (void)saveAllowedDistancePerYear:(NSString *)distance {
    VehicleList *vehicles = [VehicleList sharedInstance];
    Vehicle *vehicle = [vehicles currentVehicle];
    vehicle.allowedDistancePerYear = [distance integerValue];
    [self refreshData];
    [vehicles save];
}

- (void)saveLeaseStartDate:(NSDate *)date {
    VehicleList *vehicles = [VehicleList sharedInstance];
    Vehicle *vehicle = [vehicles currentVehicle];
    vehicle.leaseStartDate = date;
    [self refreshData];
    [vehicles save];
}


@end