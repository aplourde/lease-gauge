//
//  Vehicle.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Journal;

typedef enum {
    KM,
    MILES
} VehicleDistanceUnit;

@interface Vehicle : NSObject

@property(nonatomic, copy) NSString *id;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, strong) Journal *journal;
@property(nonatomic, strong) NSDate *leaseStartDate;
@property(nonatomic) NSInteger allowedDistancePerYear;
@property(nonatomic) VehicleDistanceUnit distanceUnit;
@property(nonatomic) NSInteger leaseDurationInMonths;
@property(nonatomic) NSInteger startingDistance;

- (NSDictionary *)toDictionary;

- (id)initWithDictionary:(NSDictionary *)dictionary;
@end
