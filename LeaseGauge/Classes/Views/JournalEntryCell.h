//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JournalEntryCell : UITableViewCell {
    IBOutlet UILabel *dateLabel;
    IBOutlet UILabel *valueLabel;
}
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) UILabel *valueLabel;
@end