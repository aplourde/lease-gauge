//
// Created by Anthony Plourde on 2014-05-04.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "EditDateValueViewController.h"
#import "InfoViewController.h"


@implementation EditDateValueViewController {

}
@synthesize titleLabel;

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-dark-leather"]];
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (IBAction)save:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.delegate performSelector:self.callback withObject:self.datePicker.date];
}

@end