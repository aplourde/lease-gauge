import urllib2
from BeautifulSoup import BeautifulSoup

userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14"

for year in range(2000,2015):
    makeForYearUrl = "http://www.thevehiclelist.com/demo14/regular/make.php?year=%i" % year
    makeForYearString = urllib2.urlopen(urllib2.Request(makeForYearUrl, headers={"User-Agent":userAgent})).read()
    makeForYearDoc = BeautifulSoup(makeForYearString)
    for makeOption in makeForYearDoc.findAll("option"):
        make = makeOption["value"]
        if make == '0':
            continue
        modelForMakeAndYearUrl = "http://www.thevehiclelist.com/demo14/regular/model.php?year=%i&make=%s" % (year, make)
        modelForMakeAndYearString = urllib2.urlopen(urllib2.Request(modelForMakeAndYearUrl, headers={"User-Agent":userAgent})).read()
        modelForMakeAndYearDoc = BeautifulSoup(modelForMakeAndYearString)
        for modelOption in modelForMakeAndYearDoc.findAll("option"):
            model = modelOption["value"]
            if model == '0':
                continue
            print "%i,%s,%s" % (year, make, model)