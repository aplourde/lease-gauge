//
// Created by Anthony Plourde on 2014-05-10.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "PieChart.h"

CGFloat RadiansToDegrees(CGFloat radians) {
    return (CGFloat) (radians * 180 / M_PI);
};
CGFloat DegreesToRadians(CGFloat degrees) {
    return (CGFloat) (degrees * M_PI / 180);
}


@implementation PieChart {

}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.color = [UIColor colorWithRed:0 green:122.0/255.0 blue:1 alpha:1];
    }

    return self;
}


- (void)setPercentage:(CGFloat)percentage {
    _percentage = percentage;
    [self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect {

    CGContextRef c = UIGraphicsGetCurrentContext();

    CGContextSetLineWidth(c, 10);

    CGFloat radius = MIN(self.frame.size.width, self.frame.size.height) / 2 - 10;
    CGFloat startAngle = 0;
    CGPoint center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    if (self.percentage > 0) {
        CGFloat endAngle = DegreesToRadians(self.percentage * 360);
        CGContextSetStrokeColorWithColor(c, self.color.CGColor);
        CGContextAddArc(c, center.x, center.y, radius, startAngle, endAngle, NO);
        CGContextDrawPath(c, kCGPathStroke);
    }
    CGFloat endAngle = DegreesToRadians(360);
    CGContextSetStrokeColorWithColor(c, [self.color colorWithAlphaComponent:0.3].CGColor);
    CGContextAddArc(c, center.x, center.y, radius, startAngle, endAngle, NO);
    CGContextDrawPath(c, kCGPathStroke);

}

@end