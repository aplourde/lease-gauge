//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JournalViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    IBOutlet UIButton *addButton;
    IBOutlet UITextField *newEntryTextField;
    IBOutlet UITableView *journalTableView;
}

- (IBAction)addJournalEntry:(id)sender;

@end