//
//  NameViewController.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardStep.h"

@interface NameViewController : WizardStep <UITextFieldDelegate> {
    IBOutlet UILabel *titleLabel;
    IBOutlet UITextField *nameTextField;
    IBOutlet UIButton *nextButton;
}

- (IBAction)nameChanged:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)back:(id)sender;

@end
