//
// Created by Anthony Plourde on 2014-04-30.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JournalEntry : NSObject

@property(nonatomic, strong) NSDate *date;
@property(nonatomic, assign) NSInteger value;

- (id)toDictionary;

- (id)initWithDictionary:(NSDictionary *)dictionary;
@end