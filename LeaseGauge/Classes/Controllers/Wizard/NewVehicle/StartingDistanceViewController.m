//
//  StartingDistanceViewController.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "StartingDistanceViewController.h"
#import "WizardController.h"
#import "Vehicle.h"
#import "NSString+IsNumeric.h"

@interface StartingDistanceViewController ()

@end

@implementation StartingDistanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startingDistanceChanged:(id)sender {
    [self setStartingDistance];
}

- (void)setStartingDistance
{
    Vehicle *vehicle = controller.buildingObject;
    if (distanceTextField.text.length > 0 && [distanceTextField.text isNumeric]) {
        vehicle.startingDistance = distanceTextField.text.integerValue;
    } else {
        vehicle.startingDistance = 0;
    }
}

- (IBAction)next:(id)sender {
    [self setStartingDistance];
    [controller goForward];
}

- (IBAction)back:(id)sender {
    [controller goBack];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


@end
