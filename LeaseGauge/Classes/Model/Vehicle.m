//
//  Vehicle.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "Vehicle.h"
#import "Journal.h"

@implementation Vehicle

- (id)init {
    self = [super init];
    if (self) {
        self.journal = [[Journal alloc] init];
    }

    return self;
}


- (NSDictionary *)toDictionary {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:self.leaseStartDate forKey:@"leaseStartDate"];
    [dictionary setObject:[NSNumber numberWithInteger:self.leaseDurationInMonths] forKey:@"leaseDurationInMonths"];
    [dictionary setObject:[NSNumber numberWithInteger:self.allowedDistancePerYear] forKey:@"allowedDistancePerYear"];
    [dictionary setObject:[NSNumber numberWithInteger:self.distanceUnit] forKey:@"distanceUnit"];
    [dictionary setObject:self.id forKey:@"id"];
    [dictionary setObject:self.name forKey:@"name"];
    [dictionary setObject:[NSNumber numberWithInteger:self.startingDistance] forKey:@"startingDistance"];
    [dictionary setObject:[self.journal toDictionaryArray] forKey:@"journal"];
    return dictionary;
}

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [self init];
    if (self) {
        self.id = [dict objectForKey:@"id"];
        self.startingDistance = [[dict objectForKey:@"startingDistance"] integerValue];
        self.name = [dict objectForKey:@"name"];
        self.distanceUnit = (VehicleDistanceUnit) [[dict objectForKey:@"distanceUnit"] integerValue];
        self.allowedDistancePerYear = [[dict objectForKey:@"allowedDistancePerYear"] integerValue];
        self.leaseDurationInMonths = [[dict objectForKey:@"leaseDurationInMonths"] integerValue];
        self.leaseStartDate = [dict objectForKey:@"leaseStartDate"];
        self.journal = [[Journal alloc] initWithDictionaryArray:[dict objectForKey:@"journal"]];
    }

    return self;
}
@end
