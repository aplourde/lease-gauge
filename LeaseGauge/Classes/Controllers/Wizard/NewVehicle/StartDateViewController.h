//
//  StartDateViewController.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardStep.h"

@interface StartDateViewController : WizardStep {
    IBOutlet UILabel *titleLabel;
    IBOutlet UIDatePicker *startDatePicker;
    IBOutlet UIButton *doneButton;
}

- (IBAction)startDateChanged:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)back:(id)sender;

@end
