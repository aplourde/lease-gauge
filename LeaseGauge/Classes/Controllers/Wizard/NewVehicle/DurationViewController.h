//
//  LeaseTermViewController.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardStep.h"

@interface DurationViewController : WizardStep <UITextFieldDelegate> {
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *monthUnitLabel;
    IBOutlet UITextField *durationTextField;
    IBOutlet UIButton *nextButton;
}

- (IBAction)durationChanged:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)back:(id)sender;

@end
