//
//  DashboardViewController.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "DashboardViewController.h"
#import "VehicleList.h"
#import "Vehicle.h"
#import "Journal.h"
#import "JournalEntry.h"
#import "EditViewController.h"
#import "VehicleListViewController.h"
#import "PieChart.h"

@interface DashboardViewController ()

@end

@implementation DashboardViewController {
    EditViewController *editViewController;
    VehicleListViewController *vehicleListView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad {
    scrollView.frame = CGRectMake(0, 65, 320, 200);
    scrollView.contentSize = CGSizeMake(320, 800);
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)welcomeDone {
    [self refreshData];
}

- (void)refreshData {

    Vehicle *vehicle = [[VehicleList sharedInstance] currentVehicle];

    NSInteger currentDistance = vehicle.journal.latestEntry.value;
    NSInteger totalDistance = vehicle.allowedDistancePerYear * vehicle.leaseDurationInMonths / 12;

    NSDate *todayDate = [NSDate date];
    NSDate *startDate = vehicle.leaseStartDate;
    NSDate *endDate = [self calculateLeaseEndDate:vehicle];

    CGFloat timePercentage = [self calculateTimeProgressionWithStartDate:startDate endDate:endDate];
    CGFloat distancePercentage = (CGFloat) (currentDistance - vehicle.startingDistance) / totalDistance;

    NSInteger expectedDistance = (NSInteger) (totalDistance * timePercentage) + vehicle.startingDistance;

    NSString *unitString = [self unitAsString:vehicle.distanceUnit];
    if (currentDistance > expectedDistance) {
        statusLabel.text = [NSString stringWithFormat:@"%i %@ over normal", currentDistance - expectedDistance, unitString];
        statusLabel.textColor = [UIColor orangeColor];
    } else {
        statusLabel.text = [NSString stringWithFormat:@"%i %@ under normal", expectedDistance - currentDistance, unitString];
        statusLabel.textColor = [UIColor greenColor];
    }

    NSString *expectedDistanceString = [NSString stringWithFormat:@"%i", expectedDistance];
    NSMutableArray *expectedDistanceStrings = [[NSMutableArray alloc] init];
    for (int i = 0; i < 6; i++) {
        if (expectedDistanceString.length > i) {
            [expectedDistanceStrings insertObject:[expectedDistanceString substringWithRange:NSMakeRange(expectedDistanceString.length - i - 1, 1)] atIndex:0];
        } else {
            [expectedDistanceStrings insertObject:@"" atIndex:0];
        }
    }
    averageLabel1.text = [expectedDistanceStrings objectAtIndex:0];
    averageLabel2.text = [expectedDistanceStrings objectAtIndex:1];
    averageLabel3.text = [expectedDistanceStrings objectAtIndex:2];
    averageLabel4.text = [expectedDistanceStrings objectAtIndex:3];
    averageLabel5.text = [expectedDistanceStrings objectAtIndex:4];
    averageLabel6.text = [expectedDistanceStrings objectAtIndex:5];


    todayDateLabel.text = [self formatDate:todayDate];
    timeLeftLabel.text = [NSString stringWithFormat:@"%@ left", [self generateTimeLeftFrom:startDate to:endDate]];
    currentDistanceLabel.text = [NSString stringWithFormat:@"%i %@", currentDistance, unitString];
    distanceLeftLabel.text = [NSString stringWithFormat:@"%i %@ left", totalDistance - (currentDistance - vehicle.startingDistance), unitString];
    timePercentageLabel.text = [NSString stringWithFormat:@"%i%%", (int) ceil(timePercentage * 100)];
    distancePercentageLabel.text = [NSString stringWithFormat:@"%i%%", (int) ceil(distancePercentage * 100)];
    averageUnitLabel.text = [unitString uppercaseString];
    vehicleNameLabel.text = vehicle.name;

    BOOL hasJournalEntries = [[vehicle journal] entryCount] > 0;
    statusLabel.hidden = !hasJournalEntries;
    lastReadLabel.hidden = !hasJournalEntries;
    currentDistanceLabel.hidden = !hasJournalEntries;
    distancePieChart.hidden = !hasJournalEntries;
    distancePercentageLabel.hidden = !hasJournalEntries;
    distanceLeftLabel.hidden = !hasJournalEntries;
    addFirstJournalEntryButton.hidden = hasJournalEntries;

    [self drawDistancePercentageChart:distancePercentage inWarning:distancePercentage > timePercentage];
    [self drawTimePercentageChart:timePercentage];
}

- (void)drawTimePercentageChart:(CGFloat)percentage {
    if (!timePercentageLabel.hidden && percentage > 0) {
        timePieChart.percentage = percentage;
    }
}

- (void)drawDistancePercentageChart:(CGFloat)percentage inWarning:(BOOL)warning {
    if (!distancePercentageLabel.hidden && percentage > 0) {
        distancePieChart.color = warning ? [UIColor orangeColor] : [UIColor greenColor];
        distancePieChart.percentage = percentage;
    }
};


- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    return [dateFormatter stringFromDate:date];
}

- (NSString *)unitAsString:(VehicleDistanceUnit)unit {
    if (unit == KM) {
        return @"km";
    } else {
        return @"miles";
    }
}

- (NSString *)generateTimeLeftFrom:(NSDate *)startDate to:(NSDate *)endDate {
    NSInteger secondsLeft = (NSInteger) [endDate timeIntervalSinceNow];
    NSInteger daysLeft = secondsLeft / (60 * 60 * 24);
    NSInteger monthsLeft = daysLeft / 30;
    if (monthsLeft > 0) {
        return [NSString stringWithFormat:@"%i months", monthsLeft];
    } else {
        return [NSString stringWithFormat:@"%i days", daysLeft];
    }
}

- (CGFloat)calculateTimeProgressionWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {

    NSInteger numberOfDaysSoFar = ((NSInteger)[[NSDate date] timeIntervalSinceDate:startDate]) / (60 * 60 * 24);
    NSInteger totalOfDays = ((NSInteger)[endDate timeIntervalSinceDate:startDate]) / (60 * 60 * 24);
    return (CGFloat) numberOfDaysSoFar/totalOfDays;
}

- (NSDate *)calculateLeaseEndDate:(Vehicle *)vehicle {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = vehicle.leaseDurationInMonths;
    return [gregorian dateByAddingComponents:components toDate:vehicle.leaseStartDate options:0];
}

- (IBAction)showEditView:(id)sender {
    if (!editViewController) {
        editViewController = [[EditViewController alloc] initWithNibName:@"EditView" bundle:nil];
    }

    [[self navigationController] pushViewController:editViewController animated:YES];
}

- (IBAction)showVehicleListView:(id)sender {
    if (!vehicleListView) {
        vehicleListView = [[VehicleListViewController alloc] initWithNibName:@"VehicleListView" bundle:nil];
    }

    NSMutableArray *vcs = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:vehicleListView atIndex:[vcs count] - 1];
    [self.navigationController setViewControllers:vcs animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    if ([[VehicleList sharedInstance] currentVehicle] != nil) {
        [self refreshData];
    } else {
        WelcomeViewController *welcomeViewController = [[WelcomeViewController alloc] initWithNibName:@"WelcomeView" bundle:nil];
        welcomeViewController.delegate = self;
        [self addChildViewController:welcomeViewController];
        [self.view addSubview:welcomeViewController.view];
    }
}

@end
