//
// Created by Anthony Plourde on 2014-05-04.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@class InfoViewController;


@interface EditChoiceValueViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {

    IBOutlet UIButton *saveButton;
    IBOutlet UILabel *titleLabel;
    IBOutlet UITableView *choiceTableView;
}

@property(nonatomic, strong) UITableView *choiceTableView;

@property(nonatomic, strong) id delegate;

@property(nonatomic, strong) NSArray *choices;

@property(nonatomic) SEL callback;

@property(nonatomic, strong) UILabel *titleLabel;

- (IBAction)save:(id)sender;

@end