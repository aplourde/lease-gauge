//
//  YearlyDistanceViewController.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardStep.h"

@interface YearlyDistanceViewController : WizardStep <UITextFieldDelegate> {
    IBOutlet UILabel *titleLabel;
    IBOutlet UITextField *distanceTextField;
    IBOutlet UIButton *kmButton;
    IBOutlet UIButton *milesButton;
    IBOutlet UIButton *nextButton;
}

- (IBAction)km:(id)sender;
- (IBAction)miles:(id)sender;
- (IBAction)yearlyDistanceChanged:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)back:(id)sender;

@end
