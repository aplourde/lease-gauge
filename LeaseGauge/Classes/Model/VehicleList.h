//
//  VehicleList.h
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Vehicle;

@interface VehicleList : NSObject

+ (VehicleList *)sharedInstance;

- (int)count;

- (NSArray *)all;

- (void)addVehicle:(Vehicle *)vehicle;

- (Vehicle *)currentVehicle;

- (void)setCurrentVehicle:(Vehicle *)cv;

- (void)save;

- (Vehicle *)vehicleAtIndex:(NSInteger)index;

- (NSInteger)currentVehicleIndex;

- (void)removeVehicleAtIndex:(NSInteger)index;
@end
