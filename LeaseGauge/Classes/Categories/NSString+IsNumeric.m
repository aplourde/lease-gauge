//
// Created by Anthony Plourde on 2014-05-11.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "NSString+IsNumeric.h"


@implementation NSString (IsNumeric)

- (BOOL)isNumeric {
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:self];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}

@end