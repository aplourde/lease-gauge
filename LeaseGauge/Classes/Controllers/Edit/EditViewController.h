//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface EditViewController : UIViewController {

    IBOutlet UIButton *journalButton;
    IBOutlet UIButton *infoButton;
    IBOutlet UIView *contentView;
    IBOutlet UILabel *vehicleName;
}

- (IBAction)showJournal:(id)sender;

- (IBAction)showInfo:(id)sender;

- (IBAction)close:(id)sender;

@end