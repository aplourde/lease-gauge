//
// Created by Anthony Plourde on 2014-05-03.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "EditTextValueViewController.h"
#import "NSString+IsNumeric.h"


@implementation EditTextValueViewController {

}

@synthesize valueTextField;

@synthesize titleLabel;

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-dark-leather"]];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction)save:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    if (self.valueTextField.text.length > 0 && (self.valueTextField.keyboardType != UIKeyboardTypeNumberPad || [self.valueTextField.text isNumeric])) {
        [self.delegate performSelector:self.callback withObject:self.valueTextField.text];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


@end