//
// Created by Anthony Plourde on 2014-04-30.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "JournalEntry.h"


@implementation JournalEntry {

}
- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.date = [dictionary objectForKey:@"date"];
        self.value = [[dictionary objectForKey:@"value"] integerValue];
    }
    return self;
}

- (id)toDictionary {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSNumber numberWithInteger:self.value] forKey:@"value"];
    [dict setObject:self.date forKey:@"date"];
    return dict;
}
@end