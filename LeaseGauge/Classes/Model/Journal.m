//
// Created by Anthony Plourde on 2014-04-30.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "Journal.h"
#import "JournalEntry.h"


@implementation Journal {
    NSMutableArray *entries;
}

- (id)init {
    self = [super init];
    if (self) {
        entries = [[NSMutableArray alloc] init];
    }

    return self;
}

- (void)addEntry:(JournalEntry *)entry {
    [entries insertObject:entry atIndex:0];
}

- (id)initWithDictionaryArray:(NSArray*)dictArray {
    self = [self init];
    if (self) {
        for(NSDictionary *dict in dictArray) {
            [entries addObject:[[JournalEntry alloc]initWithDictionary:dict]];
        }
    }

    return self;
}

- (JournalEntry *)latestEntry {
    JournalEntry *latestEntrySoFar = nil;
    for (JournalEntry *entry in entries) {
        if (!latestEntrySoFar || [entry.date compare:latestEntrySoFar.date] > 0) {
            latestEntrySoFar = entry;
        }
    }
    return latestEntrySoFar;
}

- (NSArray*)toDictionaryArray {
    NSMutableArray *dictArray = [[NSMutableArray alloc] init];
    for (JournalEntry *entry in entries) {
        [dictArray addObject:[entry toDictionary]];
    }
    return dictArray;
}

- (NSUInteger)entryCount {
    return entries.count;
}

- (JournalEntry *)entryAtIndex:(NSInteger)index {
    return [entries objectAtIndex:(NSUInteger) index];
}

- (void)removeEntryAtIndex:(NSInteger)index {
    [entries removeObjectAtIndex:(NSUInteger) index];
}
@end