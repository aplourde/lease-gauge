//
// Created by Anthony Plourde on 2014-05-10.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PieChart : UIView
@property(nonatomic) CGFloat percentage;
@property(nonatomic, strong) UIColor *color;
@end