//
//  WelcomeView.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-29.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "WelcomeViewController.h"
#import "WizardController.h"
#import "NewVehicleWizard.h"
#import "Vehicle.h"
#import "AppDelegate.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController {
    NewVehicleWizard *wizard;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-dark-leather"]];
    // Do any additional setup after loading the view.
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)add:(id)sender {
    Vehicle *vehicle = [[Vehicle alloc] init];
    wizard = [[NewVehicleWizard alloc] init];
    wizard.buildingObject = vehicle;
    wizard.wizardDelegate = self;
    [self presentViewController:wizard animated:YES completion:NULL];
}

- (void)wizardDone:(Vehicle *)builtObject {
    [self dismissViewControllerAnimated:YES completion:NULL];
    [self removeFromParentViewController];
    [self.view removeFromSuperview];
    [self.delegate welcomeDone];
}

- (void)wizardCanceled {
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
