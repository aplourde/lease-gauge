//
// Created by Anthony Plourde on 2014-05-03.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface EditTextValueViewController : UIViewController <UITextFieldDelegate> {

    IBOutlet UIButton *saveButton;
    IBOutlet UILabel *titleLabel;
    IBOutlet UITextField *valueTextField;
}

@property(nonatomic, strong) UITextField *valueTextField;

@property(nonatomic, strong) id delegate;

@property(nonatomic) SEL callback;

@property(nonatomic, strong) UILabel *titleLabel;

- (IBAction)save:(id)sender;

@end