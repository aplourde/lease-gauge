//
// Created by Anthony Plourde on 2014-05-02.
// Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "JournalViewController.h"
#import "VehicleList.h"
#import "Vehicle.h"
#import "Journal.h"
#import "JournalEntry.h"
#import "JournalEntryCell.h"
#import "NSString+IsNumeric.h"


@implementation JournalViewController {

}

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-dark-leather"]];
    newEntryTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Entry Here" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [journalTableView reloadData];
}

- (IBAction)addJournalEntry:(id)sender {

    if (newEntryTextField.text.length == 0 || ![newEntryTextField.text isNumeric]) {
        return;
    }

    JournalEntry *newEntry = [[JournalEntry alloc] init];
    newEntry.date = [NSDate date];
    newEntry.value = [newEntryTextField.text integerValue];
    [[[[VehicleList sharedInstance] currentVehicle] journal] addEntry:newEntry];
    [[VehicleList sharedInstance] save];

    newEntryTextField.text = nil;
    [newEntryTextField resignFirstResponder];

    [journalTableView beginUpdates];
    [journalTableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationAutomatic];
    [journalTableView endUpdates];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [[[[VehicleList sharedInstance] currentVehicle] journal] entryCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    JournalEntry *entry = [[[[VehicleList sharedInstance] currentVehicle] journal] entryAtIndex:indexPath.row];
    JournalEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JournalEntryCell"];
    if (!cell) {
        UIViewController *temporaryController = [[UIViewController alloc] initWithNibName:@"JournalEntryCell" bundle:nil];
        cell = (JournalEntryCell *) temporaryController.view;
    }
    cell.valueLabel.text = [NSString stringWithFormat:@"%i", entry.value];
    cell.dateLabel.text = [self formatDate:entry.date];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    return [dateFormatter stringFromDate:date];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {

        [[[[VehicleList sharedInstance] currentVehicle] journal] removeEntryAtIndex:indexPath.row];
        [[VehicleList sharedInstance] save];

        [journalTableView beginUpdates];
        [journalTableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        [journalTableView endUpdates];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end