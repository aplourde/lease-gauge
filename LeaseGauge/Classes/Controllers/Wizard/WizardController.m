//
//  WizardController.m
//  LeaseGauge
//
//  Created by Anthony Plourde on 2014-04-30.
//  Copyright (c) 2014 Anthony Plourde. All rights reserved.
//

#import "WizardController.h"
#import "WizardStep.h"

@implementation WizardController {

}
@synthesize wizardDelegate;
@synthesize buildingObject;

- (void)viewDidLoad {
    self.navigationBar.hidden = YES;
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (WizardStep *)currentStep {
    return [steps objectAtIndex:currentStepIndex];
}

- (BOOL)isFirstStep {
    return 0 == currentStepIndex;
}

- (BOOL)isLastStep {
    return [steps count] - 1 == currentStepIndex;
}


- (void)goForward {
    if (![self isLastStep]) {
        currentStepIndex++;
        [self pushViewController:[self currentStep] animated:YES];
    } else {
        [self done];
    }
}

- (void)goBack {
    if (![self isFirstStep]) {
        currentStepIndex--;
        [self popViewControllerAnimated:YES];
    } else {
        [self cancel];
    }
}

- (NSInteger)stepCount {
    return [steps count];
}

- (void)cancel {
    [self.wizardDelegate wizardCanceled];
}

- (void)done {
    [self.wizardDelegate wizardDone:self.buildingObject];
}

@end
